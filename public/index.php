<?php

//
error_reporting(E_ALL);

//
chdir(dirname(__DIR__));

include_once('core/library/Ss/Application.php');

try {
	$bootstrap = new \Ss\Application();

	echo $bootstrap->start();
} catch(Exception $e) {
	echo $e->getMessage();
}


